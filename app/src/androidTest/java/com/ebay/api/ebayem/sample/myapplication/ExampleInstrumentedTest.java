package com.ebay.api.ebayem.sample.myapplication;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.ebay.api.client.buy.BrowseApi;
import com.ebay.api.client.buy.model.SearchPagedCollection;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.ebay.api.ebayem.sample.myapplication", appContext.getPackageName());
    }


    @Test
    public void swaggerAPIBrowseTest() throws Exception {
        BrowseApi browseApi = new BrowseApi();
        //browseApi.setBasePath("http://10.254.42.106:8888");
        browseApi.getInvoker().setApiKey("Bearer v^1.1#i^1#f^0#p^1#I^3#r^0#t^H4sIAAAAAAAAAOVXa2wUVRTuttsqQhURRbEhm5GI0MzMnd2Z3dkJu3ELLWykD9naSAmPedxpB2ZnJnPv0K5/KFWJNBqtiUEhxvKIEYMRfxiiCfhK8AcqJNTGgCERMRSIikgk+kO8s13KtipFWA2J82fmnnPuud/5zjl37gU9VZPmbVy88WJ14JbygR7QUx4IcJPBpKrK2tsrymdWloEig8BAz+yeYG/F8HwkZ01HWgqRY1sIhrqzpoWkvDBBea4l2TIykGTJWYgkrEqZVOMSKcwAyXFtbKu2SYXSCxOUHFNFnguLMK4KQFBEIrUu+2y1E1RcFMJyRIMiHwe6HAsTPUIeTFsIyxZOUGHAxWgQobloK+AkTpAEnuH5aDsVaoMuMmyLmDCASubhSvm5bhHWq0OVEYIuJk6oZDrVkGlOpRfWN7XOZ4t8JQs8ZLCMPTR2tMDWYKhNNj149WVQ3lrKeKoKEaLY5MgKY51KqctgrgP+CNWKLod1novHopoGoVASKhtsNyvjq+PwJYZG63lTCVrYwLmJGCVsKGugigujJuIivTDkvx71ZNPQDegmqPq61LJUSwuVfMTLyi5EMg2iikbzQkymlbAeo6HAiarCkU8ZFBYZ8VSgeNwqC2xLM3zCUKjJxnWQIIbjeQFFvBCjZqvZTenYR1NsFynwF4kL7X5CRzLo4U7LzynMEhJC+eHE7I/Oxtg1FA/DUQ/jFXl6SJ4dx9Co8cp8HRZKpxslqE6MHYllu7q6mK4IY7sdbBgAjn28cUlG7YRZmSrY+r3ejYyJJ9BGPhQVkpnIkHDOIVi6SZ0SAFYHlQzzHB+JFXgfCys5XvonQVHM7NhuKFV3iKogRgUQCQuxmMADrhTdkSwUKOvjgIqco0mZroXYMWUV0iqpMy8LXUOTIoIejog6pLVoXKf5uK7TiqBFaU6HEECoKGpc/L80ybWW+QLTIMpWUmYlq/WS1PliG2GoXWud/2VoGdV2YIttGmruP43N7/UJ44u4Wovs4lydlyPjDDRN8rqhcNV8JleVcNMqSSL/Qb9cX9yGjG+uiDlejEfD0bjA31hc5BBzU8Wl2lnG334ZV3aw7TIEmmNCxJDs2p5LDl1Ms/8zbrXXQotsb9i1TRO6bdwNsYD8Jr65ePDnI+JAdgwmuGFPnhLCDWvLJHpfuioPOlRs93dGrOLlmA4PIkyAaND9F37s7NgrRrIs/3C9gX2gN/AeuaWAGKC5WjC3quKxYMUUChkYktxammJ3M4asM8josMgJ2oXMWphzZMMtrwo0fvvssvVFl5uBFeDe0evNpApuctFdB9Rc0VRyd8yo5mIgwkUBxwkC3w4euKINcvcEp+OnqfXvn2/0zgwumtNZM41tOd/vgepRo0CgsizYGyg7dLLvyI9PfvLgx7+nT28obxua+unx6ctXPXSs9qXQFyd2rdhrnt259exKfVNbYuW7TU367M/Vo2/1LVf7h1PBHUNTpy3d2VRfP9iwDTVKZ05uX/TR4e+/ao8E3+n/6QNjB/vGwxeFmXvt17fd3bsn3L1buDQ0WLM9eCGHjq+LHHqxpvOpLc65z34ZmDEdvvzq3DcXrz6y/7S235kmDk3pm6Muue2FWw+cqz116UD9Mw2dc/fedefUYSc2e0vZc6nXBH7evlM7TlcPtyZbBuuet63dqaDz4YFdR5VFx3+4PyWkvu4/NevClhNb69vc9t+++7lv3eYvvwkdS7NPBBrAuYO/3rf5lVmHV4ub1hx8eySNfwBI5HBxdg4AAA==");


        try {
            SearchPagedCollection searchPagedCollection = browseApi.searchForItems("iphone", null, null, null, null, null);
            assert searchPagedCollection.getItemSummaries().get(1) != null;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
