package com.ebay.api.ebayem.sample.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.ebay.api.ebayem.EBayAppConfig;
import com.ebay.api.ebayem.EBayEM;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG =  "MainActivity";

    EditText searchEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.searchEditText = (EditText)findViewById(R.id.search_query);
        View searchButton = findViewById(R.id.search_button);
        searchButton.setOnClickListener(this);

        EBayAppConfig eBayAppConfig = getEBayAppConfig();
        EBayEM.getInstance().initialize(getApplicationContext(),eBayAppConfig);
    }

    public void onClick(View view) {
        String searchQuery = searchEditText.getText().toString();
        Log.d(TAG,"searching for " + searchQuery);
        EBayEM.getInstance().search(view.getContext(),searchQuery);
    }


    /**
     * eBay App Config holds the eBay developer config like client id etc.
     * These IDs are vulnerable for hacking
     *
     * This sample app stores these as string resources.
     * But production Application should consider better mechanisms secure these ids.
     *
     * @return EBayAppConfig
     */
    EBayAppConfig getEBayAppConfig(){
        String clientId = getResources().getString(R.string.ebay_client_id);
        String clientSecret = getResources().getString(R.string.ebay_client_secret);
        Boolean isProduction = getResources().getBoolean(R.bool.ebay_is_production);
        return new EBayAppConfig(clientId,clientSecret,isProduction);
    }


}
