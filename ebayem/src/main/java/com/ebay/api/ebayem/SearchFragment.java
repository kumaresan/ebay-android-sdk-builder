package com.ebay.api.ebayem;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import java.util.List;

import com.ebay.api.client.buy.BrowseApi;
import com.ebay.api.client.buy.model.ItemSummary;
import com.ebay.api.client.buy.model.SearchPagedCollection;

import static com.ebay.api.ebayem.Constants.ARG_SEARCH_QUERY;

/**
 * The default search fragment the Search Fragment.
 * <p>
 * Created by @Kumaresan on 3/8/2017.
 */

public class SearchFragment extends Fragment implements Response.Listener<SearchPagedCollection>, Response.ErrorListener {
    private static final String TAG = "SearchFragment";

    View searchView;
    private RecyclerView recyclerView;
    OnItemSelectedListener onItemSelectedListener;

    // Container Activity must implement this interface
    public interface OnItemSelectedListener {
        public void onItemSelected(String itemId);
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */

    public SearchFragment() {
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onItemSelectedListener = (OnItemSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnItemSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String searchQuery = getArguments().getString(ARG_SEARCH_QUERY);

        Log.d(TAG, "onCreateView " + searchQuery);

        this.searchView = inflater.inflate(R.layout.fragment_search, container, false);

//        this.textView = ((TextView) this.searchView.findViewById(R.id.ebay_search_text));
//        this.textView.setText("Searching for " + searchQuery);

        this.recyclerView = (RecyclerView) this.searchView.findViewById(R.id.search_item_list);
        assert this.recyclerView != null;


        EBayEM.getInstance().getBrowseApi().searchForItems(searchQuery, null, null, null, null, null, this, this);
        return searchView;
    }


    private void hideProgress(View view) {
        view.findViewById(R.id.progress_bar).setVisibility(View.GONE);
    }

    private void showError(View view, String text) {
        ((TextView) this.searchView.findViewById(R.id.error_message)).setText(text);
        view.findViewById(R.id.error_view).setVisibility(View.VISIBLE);
    }

    public void onResponse(SearchPagedCollection searchPagedCollection) {
        Log.d(TAG, searchPagedCollection.toString());
        hideProgress(this.searchView);
        this.recyclerView.setVisibility(View.VISIBLE);
        this.recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(searchPagedCollection.getItemSummaries(), this.onItemSelectedListener));
    }

    public void onErrorResponse(VolleyError volleyError) {
        Log.d(TAG, volleyError.toString());
        hideProgress(this.searchView);
        showError(this.searchView, volleyError.getMessage());
        Toast.makeText(getActivity().getApplicationContext(), "Error! @ " + volleyError.getMessage(), Toast.LENGTH_LONG).show();
    }


    class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final List<ItemSummary> itemSummaries;
        private final OnItemSelectedListener onItemSelectedListener;

        SimpleItemRecyclerViewAdapter(List<ItemSummary> itemSummaries, OnItemSelectedListener onItemSelectedListener) {
            this.itemSummaries = itemSummaries;
            this.onItemSelectedListener = onItemSelectedListener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_search_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.setItemSummary(itemSummaries.get(position), this.onItemSelectedListener);
        }

        @Override
        public int getItemCount() {
            if (itemSummaries == null) return 0;
            return itemSummaries.size();
        }


        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private static final String Tag = "ViewHolder";

            private OnItemSelectedListener onItemSelectedListener;
            final View mView;
            final ImageView mItemImageView;
            final TextView mTitleTextView;
            final TextView mPriceTextView;
            final TextView mConditionTextView;
            final TextView mSellerTextView;

            private ItemSummary mItem;

            ViewHolder(View view) {
                super(view);
                mView = view;

                mItemImageView = (ImageView) view.findViewById(R.id.item_image);
                mTitleTextView = (TextView) view.findViewById(R.id.title);
                mPriceTextView = (TextView) view.findViewById(R.id.price);
                mConditionTextView = (TextView) view.findViewById(R.id.condition);
                mSellerTextView = (TextView) view.findViewById(R.id.seller);
            }

            public void setItemSummary(ItemSummary mItem, OnItemSelectedListener onItemSelectedListener) {
                this.mItem = mItem;
                this.onItemSelectedListener = onItemSelectedListener;

                if (this.mItem.getImage() == null || this.mItem.getImage().getImageUrl() == null) {
                    Picasso.with(mView.getContext()).load(R.drawable.blank_item).into(this.mItemImageView);
                } else {
                    Picasso.with(mView.getContext()).load(this.mItem.getImage().getImageUrl()).into(this.mItemImageView);
                }

                this.mTitleTextView.setText(this.mItem.getTitle());
                this.mPriceTextView.setText("$ " + this.mItem.getPrice().getValue());
                this.mConditionTextView.setText(" | " + this.mItem.getCondition());

                if (this.mItem.getSeller() != null && this.mItem.getSeller().getUsername() != null) {
                    this.mSellerTextView.setText(" | " + this.mItem.getSeller().getUsername());
                }

                this.mView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick");
                this.onItemSelectedListener.onItemSelected(mItem.getItemId());
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mItem + "'";
            }
        }
    }

}
