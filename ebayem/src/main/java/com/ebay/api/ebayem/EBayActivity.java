package com.ebay.api.ebayem;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class EBayActivity extends AppCompatActivity implements SearchFragment.OnItemSelectedListener {
    private static final String TAG = "EBayActivity";

    InstanceState instanceState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebay);

        if(savedInstanceState == null){
            this.instanceState = new InstanceState(getIntent());
        } else {
            this.instanceState = new InstanceState(savedInstanceState);
        }

        if(instanceState.action == Constants.ACTION_SEARCH){

            Fragment searchFragment = EBayEM.getInstance().getSearchHelper().getSearchFragment(this, this.instanceState.searchQuery);
            showFragment(searchFragment,false);

        } else if (this.instanceState.action == Constants.ACTION_VIEW_ITEM) {

            Fragment itemFragment = EBayEM.getInstance().getItemHelper().getItemFragment(this, this.instanceState.itemId);
            showFragment(itemFragment,false);

        } else {
            // TODO show good message
            this.showError("Invalid Intent");
        }
    }

    void showError(String message){
        throw new RuntimeException("EBayActivity.showError yet to be implemented");
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        this.instanceState.saveInstanceState(outState);
        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    private void showFragment(Fragment fragment, Boolean addToBackStack){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.ebay_fragment_container,fragment);
        if(addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }


    @Override
    public void onItemSelected(String itemId){
        Log.d(TAG, "onItemSelected");
        Fragment itemFragment = EBayEM.getInstance().getItemHelper().getItemFragment(this, itemId);
        showFragment(itemFragment,true);
    }

    /**
     * Util Class to manage the state of the Activity
     */
    private static class InstanceState {
        Integer action;
        String searchQuery;
        String itemId;

        public InstanceState(@NonNull Intent intent){
            this.action = intent.getIntExtra(Constants.ARG_ACTION, Constants.ACTION_SEARCH);
            this.searchQuery = intent.getStringExtra(Constants.ARG_SEARCH_QUERY);
            this.itemId = intent.getStringExtra(Constants.ARG_ITEM_ID);
        }

        public InstanceState(@NonNull Bundle savedInstanceState){
            this.action = savedInstanceState.getInt(Constants.ARG_ACTION);
            this.searchQuery = savedInstanceState.getString(Constants.ARG_SEARCH_QUERY);
            this.itemId = savedInstanceState.getString(Constants.ARG_ITEM_ID);
        }

        public void saveInstanceState(@NonNull Bundle outState){
            outState.putInt(Constants.ARG_ACTION, this.action);
            outState.putString(Constants.ARG_SEARCH_QUERY, this.searchQuery);
            outState.putString(Constants.ARG_ITEM_ID, this.itemId);
        }
    }

}
