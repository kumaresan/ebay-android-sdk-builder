package com.ebay.api.ebayem;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.app.Fragment;

/**
 * Created by @Kumaresan on 3/8/2017.
 */

public class ItemHelper {

    /**
     * Show the item on view
     *
     * @param context
     * @param itemId
     * @return
     */
    public Intent getItemIntent(Context context, String itemId) {
        return new Builder(context,itemId).buildIntent();
    }

    public Fragment getItemFragment(Context context, String itemId){
        return new Builder(context,itemId).buildFragment();
    }

    public class Builder {
        final Context context;
        final String itemId;

        public Builder(@NonNull Context context, @NonNull String itemId) {
            this.context = context;
            this.itemId = itemId;
        }

        public Intent buildIntent() {
            Intent intent = new Intent(context, EBayActivity.class);
            intent.putExtra(Constants.ARG_ACTION, Constants.ACTION_VIEW_ITEM);
            intent.putExtra(Constants.ARG_ITEM_ID, this.itemId);
            return intent;
        }

        public ItemFragment buildFragment(){
            ItemFragment itemFragment = new ItemFragment();

            Bundle bundle = new Bundle();
            bundle.putString(Constants.ARG_ITEM_ID, this.itemId);
            itemFragment.setArguments(bundle);

            return itemFragment;

        }
    }

}
