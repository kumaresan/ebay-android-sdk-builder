package com.ebay.api.ebayem;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.app.Fragment;

/**
 * Created by @Kumaresan on 3/6/2017.
 */

public class SearchHelper {

    /**
     * show the search view for the given query
     *
     * @param context
     * @param query
     * @return
     */
    public Intent getSearchIntent(Context context, String query) {
        return new Builder(context,query).buildIntent();
    }

    public Fragment getSearchFragment(Context context, String query){
        return new Builder(context,query).buildFragment();
    }


    public class Builder {
        final Context context;  // TODO do we need this
        final String query;

        // TODO add other search parameters here
        // limit, offset, category, sort, filter etc.

        public Builder(@NonNull Context context, @NonNull String query) {
            this.context = context;
            this.query = query;
        }

        public Intent buildIntent() {
            Intent intent = new Intent(context, EBayActivity.class);
            intent.putExtra(Constants.ARG_ACTION, Constants.ACTION_SEARCH);
            intent.putExtra(Constants.ARG_SEARCH_QUERY, this.query);
            return intent;
        }

        public SearchFragment buildFragment(){
            SearchFragment searchFragment = new SearchFragment();

            Bundle bundle = new Bundle();
            bundle.putString(Constants.ARG_SEARCH_QUERY, this.query);
            searchFragment.setArguments(bundle);

            return searchFragment;
        }
    }

}
