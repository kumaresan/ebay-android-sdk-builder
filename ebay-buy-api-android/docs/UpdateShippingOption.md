
# UpdateShippingOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lineItemId** | **String** |  |  [optional]
**shippingOptionId** | **String** |  |  [optional]



