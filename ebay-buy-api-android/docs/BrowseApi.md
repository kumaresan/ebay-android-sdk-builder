# BrowseApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getItem**](BrowseApi.md#getItem) | **GET** /browse/v1/item/{item_id} | get details of an item
[**getItemFeed**](BrowseApi.md#getItemFeed) | **GET** /browse/v1/item_feed | get Item Feed
[**getItemGroup**](BrowseApi.md#getItemGroup) | **GET** /browse/v1/item_group/{item_group_id} | get Item Group
[**searchForItems**](BrowseApi.md#searchForItems) | **GET** /browse/v1/item_summary/search | Search


<a name="getItem"></a>
# **getItem**
> Item getItem(itemId)

get details of an item

By passing the id we can get details about the item 

### Example
```java
// Import classes:
//import com.ebay.api.client.buy.BrowseApi;

BrowseApi apiInstance = new BrowseApi();
String itemId = "itemId_example"; // String | Item Id
try {
    Item result = apiInstance.getItem(itemId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrowseApi#getItem");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemId** | **String**| Item Id |

### Return type

[**Item**](Item.md)

### Authorization

[OauthSecurity](../README.md#OauthSecurity)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getItemFeed"></a>
# **getItemFeed**
> ItemFeedResponse getItemFeed(categoryId, date, feedType, X_EBAY_C_MARKETPLACE_ID)

get Item Feed

Get Item Feed 

### Example
```java
// Import classes:
//import com.ebay.api.client.buy.BrowseApi;

BrowseApi apiInstance = new BrowseApi();
String categoryId = "categoryId_example"; // String | category id
String date = "date_example"; // String | Date format yyyymmdd
String feedType = "feedType_example"; // String | The eBay Partners Network feed type
String X_EBAY_C_MARKETPLACE_ID = "X_EBAY_C_MARKETPLACE_ID_example"; // String | The 'X-EBAY-C-MARKETPLACE-ID' header is required. EBAY-US.
try {
    ItemFeedResponse result = apiInstance.getItemFeed(categoryId, date, feedType, X_EBAY_C_MARKETPLACE_ID);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrowseApi#getItemFeed");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryId** | **String**| category id |
 **date** | **String**| Date format yyyymmdd |
 **feedType** | **String**| The eBay Partners Network feed type |
 **X_EBAY_C_MARKETPLACE_ID** | **String**| The &#39;X-EBAY-C-MARKETPLACE-ID&#39; header is required. EBAY-US. |

### Return type

[**ItemFeedResponse**](ItemFeedResponse.md)

### Authorization

[OauthSecurity](../README.md#OauthSecurity)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getItemGroup"></a>
# **getItemGroup**
> ItemGroup getItemGroup(itemGroupId)

get Item Group

By passing the group id we can get items in the given group 

### Example
```java
// Import classes:
//import com.ebay.api.client.buy.BrowseApi;

BrowseApi apiInstance = new BrowseApi();
String itemGroupId = "itemGroupId_example"; // String | Item Group Id
try {
    ItemGroup result = apiInstance.getItemGroup(itemGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrowseApi#getItemGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemGroupId** | **String**| Item Group Id |

### Return type

[**ItemGroup**](ItemGroup.md)

### Authorization

[OauthSecurity](../README.md#OauthSecurity)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="searchForItems"></a>
# **searchForItems**
> SearchPagedCollection searchForItems(q, categoryIds, limit, offset, filter, sort)

Search

Search for Items 

### Example
```java
// Import classes:
//import com.ebay.api.client.buy.BrowseApi;

BrowseApi apiInstance = new BrowseApi();
String q = "q_example"; // String | Query String
String categoryIds = "categoryIds_example"; // String | TBD better examples for the category ids
String limit = "limit_example"; // String | Limit for Pagination
String offset = "offset_example"; // String | Offset for Pagination
List<String> filter = Arrays.asList("filter_example"); // List<String> | Filter Field
List<String> sort = Arrays.asList("sort_example"); // List<String> | Sort Field
try {
    SearchPagedCollection result = apiInstance.searchForItems(q, categoryIds, limit, offset, filter, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrowseApi#searchForItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| Query String |
 **categoryIds** | **String**| TBD better examples for the category ids | [optional]
 **limit** | **String**| Limit for Pagination | [optional]
 **offset** | **String**| Offset for Pagination | [optional]
 **filter** | [**List&lt;String&gt;**](String.md)| Filter Field | [optional]
 **sort** | [**List&lt;String&gt;**](String.md)| Sort Field | [optional]

### Return type

[**SearchPagedCollection**](SearchPagedCollection.md)

### Authorization

[OauthSecurity](../README.md#OauthSecurity)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

