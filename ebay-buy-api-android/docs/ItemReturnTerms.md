
# ItemReturnTerms

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**extendedHolidayReturnsOffered** | **Boolean** |  |  [optional]
**refundMethod** | **String** |  |  [optional]
**restockingFeePercentage** | **String** |  |  [optional]
**returnInstructions** | **String** |  |  [optional]
**returnMethod** | **String** |  |  [optional]
**returnPeriod** | [**TimeDuration**](TimeDuration.md) |  |  [optional]
**returnsAccepted** | **Boolean** |  |  [optional]
**returnShippingCostPayer** | **String** |  |  [optional]



