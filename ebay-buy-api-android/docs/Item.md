
# Item

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additionalImages** | [**List&lt;Image&gt;**](Image.md) |  |  [optional]
**ageGroup** | **String** |  |  [optional]
**availabilityStatusForShipToHome** | **String** |  |  [optional]
**bidCount** | **Integer** |  |  [optional]
**brand** | **String** |  |  [optional]
**buyingOptions** | **List&lt;String&gt;** |  |  [optional]
**categoryPath** | **String** |  |  [optional]
**color** | **String** |  |  [optional]
**condition** | **String** |  |  [optional]
**currentBidPrice** | [**Amount**](Amount.md) |  |  [optional]
**description** | **String** |  |  [optional]
**energyEfficiencyClass** | **String** |  |  [optional]
**gender** | **String** |  |  [optional]
**gtin** | **String** |  |  [optional]
**image** | [**Image**](Image.md) |  |  [optional]
**itemAffiliateWebUrl** | **String** |  |  [optional]
**itemEndDate** | **String** |  |  [optional]
**itemId** | **String** |  |  [optional]
**itemLocation** | [**Address**](Address.md) |  |  [optional]
**itemWebUrl** | **String** |  |  [optional]
**localizedAspects** | [**List&lt;TypedNameValue&gt;**](TypedNameValue.md) |  |  [optional]
**marketingPrice** | [**MarketingPrice**](MarketingPrice.md) |  |  [optional]
**material** | **String** |  |  [optional]
**mpn** | **String** |  |  [optional]
**pattern** | **String** |  |  [optional]
**price** | [**Amount**](Amount.md) |  |  [optional]
**priceDisplayCondition** | **String** |  |  [optional]
**primaryItemGroupHref** | **String** |  |  [optional]
**primaryItemGroupId** | **String** |  |  [optional]
**primaryProductReviewRating** | [**ReviewRating**](ReviewRating.md) |  |  [optional]
**quantityLimitPerBuyer** | **Integer** |  |  [optional]
**quantitySold** | **Integer** |  |  [optional]
**returnTerms** | [**ItemReturnTerms**](ItemReturnTerms.md) |  |  [optional]
**seller** | [**Seller**](Seller.md) |  |  [optional]
**shippingOptions** | [**List&lt;ShippingOption&gt;**](ShippingOption.md) |  |  [optional]
**shortDescription** | **String** |  |  [optional]
**size** | **String** |  |  [optional]
**sizeSystem** | **String** |  |  [optional]
**subtitle** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**topRatedBuyingExperience** | **Boolean** |  |  [optional]
**uniqueBidderCount** | **Integer** |  |  [optional]
**warnings** | [**List&lt;ErrorDetailV3&gt;**](ErrorDetailV3.md) |  |  [optional]



