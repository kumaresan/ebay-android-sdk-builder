
# ItemSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additionalImages** | [**List&lt;Image&gt;**](Image.md) |  |  [optional]
**bidCount** | **Integer** |  |  [optional]
**buyingOptions** | **List&lt;String&gt;** |  |  [optional]
**categories** | [**List&lt;Category&gt;**](Category.md) |  |  [optional]
**condition** | **String** |  |  [optional]
**currentBidPrice** | [**Amount**](Amount.md) |  |  [optional]
**distanceFromPickupLocation** | [**TargetLocation**](TargetLocation.md) |  |  [optional]
**energyEfficiencyClass** | **String** |  |  [optional]
**image** | [**Image**](Image.md) |  |  [optional]
**itemAffiliateWebUrl** | **String** |  |  [optional]
**itemGroupHref** | **String** |  |  [optional]
**itemId** | **String** |  |  [optional]
**itemLocation** | [**Address**](Address.md) |  |  [optional]
**itemWebUrl** | **String** |  |  [optional]
**marketingPrice** | [**MarketingPrice**](MarketingPrice.md) |  |  [optional]
**pickupOptions** | [**List&lt;PickupOptionSummary&gt;**](PickupOptionSummary.md) |  |  [optional]
**price** | [**Amount**](Amount.md) |  |  [optional]
**seller** | [**Seller**](Seller.md) |  |  [optional]
**shippingOptions** | [**List&lt;ShippingOptionSummary&gt;**](ShippingOptionSummary.md) |  |  [optional]
**thumbnailImages** | [**List&lt;Image&gt;**](Image.md) |  |  [optional]
**title** | **String** |  |  [optional]
**topRatedBuyingExperience** | **Boolean** |  |  [optional]



