
# ItemFeedResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itemFeed** | [**List&lt;ItemFeed&gt;**](ItemFeed.md) |  |  [optional]



