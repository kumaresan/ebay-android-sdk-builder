
# CreateGuestCheckoutSessionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contactEmail** | **String** |  |  [optional]
**contactFirstName** | **String** |  |  [optional]
**contactLastName** | **String** |  |  [optional]
**creditCard** | [**CreditCard**](CreditCard.md) |  |  [optional]
**lineItemInputs** | [**List&lt;LineItemInput&gt;**](LineItemInput.md) |  |  [optional]
**shippingAddress** | [**Address**](Address.md) |  |  [optional]



