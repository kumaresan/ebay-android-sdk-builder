
# ReviewRating

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**averageRating** | **String** |  |  [optional]
**ratingHistograms** | [**List&lt;RatingHistogram&gt;**](RatingHistogram.md) |  |  [optional]
**reviewCount** | **Integer** |  |  [optional]



