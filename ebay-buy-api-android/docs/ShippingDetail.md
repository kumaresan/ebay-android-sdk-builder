
# ShippingDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxEstimatedDeliveryDate** | **String** |  |  [optional]
**minEstimatedDeliveryDate** | **String** |  |  [optional]
**shippingCarrierName** | **String** |  |  [optional]
**shippingServiceName** | **String** |  |  [optional]



