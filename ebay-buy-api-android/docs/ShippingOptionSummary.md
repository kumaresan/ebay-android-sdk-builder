
# ShippingOptionSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shippingCost** | [**Amount**](Amount.md) |  |  [optional]
**shippingCostType** | **String** |  |  [optional]



