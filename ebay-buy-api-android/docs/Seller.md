
# Seller

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | 
**sellerAccountType** | **String** |  |  [optional]
**feedbackPercentage** | **String** | The percentage of total feedback that is positive. |  [optional]
**feedbackScore** | **Integer** | The feedback score of the seller, which is based on buyer ratings. |  [optional]



