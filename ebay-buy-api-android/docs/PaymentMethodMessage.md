
# PaymentMethodMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**legalMessage** | **String** |  |  [optional]
**requiredForUserConfirmation** | **String** |  |  [optional]



