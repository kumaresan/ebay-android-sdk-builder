
# PaymentMethodBrand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logoImage** | [**Image**](Image.md) |  |  [optional]
**paymentMethodBrandType** | **String** |  |  [optional]



