
# SearchPagedCollection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **String** |  |  [optional]
**itemSummaries** | [**List&lt;ItemSummary&gt;**](ItemSummary.md) |  |  [optional]
**brand** | **String** |  |  [optional]
**limit** | **Integer** |  |  [optional]
**next** | **String** |  |  [optional]
**offset** | **Integer** |  |  [optional]
**prev** | **String** |  |  [optional]
**total** | **Integer** |  |  [optional]
**warnings** | [**List&lt;ErrorDetailV3&gt;**](ErrorDetailV3.md) |  |  [optional]



