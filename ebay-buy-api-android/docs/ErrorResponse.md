
# ErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errors** | [**List&lt;ErrorDetailV3&gt;**](ErrorDetailV3.md) |  |  [optional]



