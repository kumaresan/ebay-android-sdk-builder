# ebay-buy-api-android

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>com.bitbucket.kumaresan</groupId>
    <artifactId>ebay-buy-api-android</artifactId>
    <version>0.0.6</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "com.bitbucket.kumaresan:ebay-buy-api-android:0.0.6"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/ebay-buy-api-android-0.0.6.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import com.ebay.api.client.buy.BrowseApi;

public class BrowseApiExample {

    public static void main(String[] args) {
        BrowseApi apiInstance = new BrowseApi();
        String itemId = "itemId_example"; // String | Item Id
        try {
            Item result = apiInstance.getItem(itemId);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling BrowseApi#getItem");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*BrowseApi* | [**getItem**](docs/BrowseApi.md#getItem) | **GET** /browse/v1/item/{item_id} | get details of an item
*BrowseApi* | [**getItemFeed**](docs/BrowseApi.md#getItemFeed) | **GET** /browse/v1/item_feed | get Item Feed
*BrowseApi* | [**getItemGroup**](docs/BrowseApi.md#getItemGroup) | **GET** /browse/v1/item_group/{item_group_id} | get Item Group
*BrowseApi* | [**searchForItems**](docs/BrowseApi.md#searchForItems) | **GET** /browse/v1/item_summary/search | Search
*OrderApi* | [**getGuestCheckoutSession**](docs/OrderApi.md#getGuestCheckoutSession) | **GET** /order/v1/guest_checkout_session/{guest_checkoutsession_id} | get details of Guest Checkout Session
*OrderApi* | [**getGuestPurchaseOrder**](docs/OrderApi.md#getGuestPurchaseOrder) | **GET** /order/v1/guest_purchase_order/{purchase_order_id} | get details of a purchase order
*OrderApi* | [**initiateGuestCheckoutSession**](docs/OrderApi.md#initiateGuestCheckoutSession) | **POST** /order/v1/guest_checkout_session/initiate | Initiate Guest Checkout Session
*OrderApi* | [**placeGuestOrder**](docs/OrderApi.md#placeGuestOrder) | **POST** /order/v1/guest_checkout_session/{guest_checkoutsession_id}/place_order | Place Guest Order
*OrderApi* | [**updateGuestLineItemQuantity**](docs/OrderApi.md#updateGuestLineItemQuantity) | **POST** /order/v1/guest_checkout_session/{guest_checkoutsession_id}/update_quantity | Update Guest Line Item Quantity
*OrderApi* | [**updateGuestPaymentInfo**](docs/OrderApi.md#updateGuestPaymentInfo) | **POST** /order/v1/guest_checkout_session/{guest_checkoutsession_id}/update_payment_info | Update Guest Payment Info
*OrderApi* | [**updateGuestShippingAddress**](docs/OrderApi.md#updateGuestShippingAddress) | **POST** /order/v1/guest_checkout_session/{guest_checkoutsession_id}/update_shipping_address | Update Guest Shipping Address
*OrderApi* | [**updateGuestShippingOption**](docs/OrderApi.md#updateGuestShippingOption) | **POST** /order/v1/guest_checkout_session/{guest_checkoutsession_id}/update_shipping_option | Update Guest Shipping Option


## Documentation for Models

 - [Address](docs/Address.md)
 - [Adjustment](docs/Adjustment.md)
 - [Amount](docs/Amount.md)
 - [BillingAddress](docs/BillingAddress.md)
 - [Category](docs/Category.md)
 - [CheckoutSessionResponse](docs/CheckoutSessionResponse.md)
 - [CreateGuestCheckoutSessionRequest](docs/CreateGuestCheckoutSessionRequest.md)
 - [CreditCard](docs/CreditCard.md)
 - [ErrorDetailV3](docs/ErrorDetailV3.md)
 - [ErrorParameterV3](docs/ErrorParameterV3.md)
 - [ErrorResponse](docs/ErrorResponse.md)
 - [GuestPurchaseOrder](docs/GuestPurchaseOrder.md)
 - [Image](docs/Image.md)
 - [Item](docs/Item.md)
 - [ItemDigest](docs/ItemDigest.md)
 - [ItemFeed](docs/ItemFeed.md)
 - [ItemFeedResponse](docs/ItemFeedResponse.md)
 - [ItemGroup](docs/ItemGroup.md)
 - [ItemReturnTerms](docs/ItemReturnTerms.md)
 - [ItemSummary](docs/ItemSummary.md)
 - [LineItem](docs/LineItem.md)
 - [LineItemInput](docs/LineItemInput.md)
 - [MarketingPrice](docs/MarketingPrice.md)
 - [PaymentInstrumentReference](docs/PaymentInstrumentReference.md)
 - [PaymentMethod](docs/PaymentMethod.md)
 - [PaymentMethodBrand](docs/PaymentMethodBrand.md)
 - [PaymentMethodMessage](docs/PaymentMethodMessage.md)
 - [PickupOptionSummary](docs/PickupOptionSummary.md)
 - [PricingSummary](docs/PricingSummary.md)
 - [ProvidedPaymentInstrument](docs/ProvidedPaymentInstrument.md)
 - [PurchaseOrderSummary](docs/PurchaseOrderSummary.md)
 - [RatingHistogram](docs/RatingHistogram.md)
 - [ReviewRating](docs/ReviewRating.md)
 - [SearchPagedCollection](docs/SearchPagedCollection.md)
 - [Seller](docs/Seller.md)
 - [ShippingDetail](docs/ShippingDetail.md)
 - [ShippingOption](docs/ShippingOption.md)
 - [ShippingOptionSummary](docs/ShippingOptionSummary.md)
 - [TargetLocation](docs/TargetLocation.md)
 - [TimeDuration](docs/TimeDuration.md)
 - [TypedNameValue](docs/TypedNameValue.md)
 - [UpdatePaymentInformation](docs/UpdatePaymentInformation.md)
 - [UpdateQuantity](docs/UpdateQuantity.md)
 - [UpdateShippingOption](docs/UpdateShippingOption.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### OauthSecurity

- **Type**: API key
- **API key parameter name**: Authorization
- **Location**: HTTP header


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author

kumaresan.manickavelu@gmail.com

